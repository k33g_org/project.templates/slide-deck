#!/bin/bash
rm index.md
for i in ./src/*.md;do cat $i >> tmp.md;done

set -o allexport; source .env; set +o allexport

envsubst < tmp.md > index.md

rm tmp.md
